
# Identity Logout Attempt:

![image 1](https://id.atlassian.com/logout?continue=https%3A%2F%2Fbitbucket.org%2Faccount%2Fsignout%2F&prompt=none&prompt=none)

```
https://id.atlassian.com/logout?continue=https%3A%2F%2Fbitbucket.org%2Faccount%2Fsignout%2F&prompt=none
```

Are you [still logged in at Identity?](https://id.atlassian.com)

# BBC Logout Attempt:


![image 2](https://bitbucket.org/account/signout/)

```
https://bitbucket.org/account/signout/
```

Are you [still logged in here?](./)