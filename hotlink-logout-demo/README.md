[https://www.gnucitizen.org/blog/persistent-csrf-and-the-hotlink-hell/](https://www.gnucitizen.org/blog/persistent-csrf-and-the-hotlink-hell/)

# This will be annoying:

## Hotlinks that attempt to log you out of BBC & Identity

Before you go, check to see if you are still logged in

* [./hotlink-logout-bbc-and-identity.md](./hotlink-logout-bbc-and-identity.md)
    * [still logged in at Identity?](https://id.atlassian.com)

